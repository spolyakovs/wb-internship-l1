package main

import (
	"testing"
)

func TestMakeAction(t *testing.T) {
	h := Human{
		name:      "human name",
		birthDate: "21.12.2012",
	}

	a := MakeAction("action name", h)

	a.ChangeName("new name") // changes Human(!!!) name, not Action name
	a.ChangeDB("31.01.2000") // changes Human.birthDate, but Action inherits it

	if a.name != "action name" {
		t.Errorf("wrong a.name: want: %v, got: %v", "action name", a.name)
	}
	if a.birthDate != "31.01.2000" {
		t.Errorf("wrong a.birthDate: want: %v, got: %v", "31.01.2000", a.birthDate)
	}
}